{
  "openapi" : "3.0.2",
  "info" : {
    "title": "UW-Madison Person API",
    "version": "1.0"
  },
  "externalDocs" : {
    "description" : "Read more about JSON:API here",
    "url" : "https://jsonapi.org/"
  },
  "servers" : [{
    "url" : "enter-url-here"
  }],
  "security" : [ {
    "OAuth2ClientCredentials" : [ ]
  } ],
  "paths" : {
    "/people" : {
      "get" : {
        "description" : "Get a collection of people.",
        "operationId" : "get people",
        "tags" : [ "people" ],
        "parameters" : [ {
          "$ref" : "#/components/parameters/filter"
        }, {
          "$ref" : "#/components/parameters/fields"
        }, {
          "$ref" : "#/components/parameters/include"
        }, {
          "$ref" : "#/components/parameters/page"
        } ],
        "responses" : {
          "200" : {
            "$ref" : "#/components/responses/people"
          },
          "400" : {
            "$ref" : "#/components/responses/bad-request"
          },
          "401" : {
            "$ref" : "#/components/responses/unauthorized"
          },
          "406" : {
            "$ref" : "#/components/responses/not-acceptable"
          },
          "429" : {
            "$ref" : "#/components/responses/too-many-requests"
          },
          "500" : {
            "$ref" : "#/components/responses/internal-server-error"
          }
        }
      }
    },
    "/people/{id}" : {
      "get" : {
        "description" : "Get a person. It is not recommended to manually build this URL or other URLs that contain path parameters, for example, by using concatenation. Instead, use the fully-qualified links returned in other resources to use this resource and other \"Get By ID\" resources.",
        "operationId" : "get person",
        "tags" : [ "people" ],
        "parameters" : [ {
          "$ref" : "#/components/parameters/id"
        }, {
          "$ref" : "#/components/parameters/include"
        }, {
          "$ref" : "#/components/parameters/fields"
        } ],
        "responses" : {
          "200" : {
            "$ref" : "#/components/responses/people-id"
          },
          "400" : {
            "$ref" : "#/components/responses/bad-request"
          },
          "401" : {
            "$ref" : "#/components/responses/unauthorized"
          },
          "404" : {
            "$ref" : "#/components/responses/not-found"
          },
          "406" : {
            "$ref" : "#/components/responses/not-acceptable"
          },
          "429" : {
            "$ref" : "#/components/responses/too-many-requests"
          },
          "500" : {
            "$ref" : "#/components/responses/internal-server-error"
          }
        }
      }
    },
    "/people/{id}/identifiers" : {
      "get" : {
        "description" : "Get identifiers for a person.",
        "operationId" : "get person identifiers",
        "tags" : [ "people" ],
        "parameters" : [ {
          "$ref" : "#/components/parameters/id"
        }, {
          "$ref" : "#/components/parameters/fields"
        } ],
        "responses" : {
          "200" : {
            "$ref" : "#/components/responses/identifiers"
          },
          "400" : {
            "$ref" : "#/components/responses/bad-request"
          },
          "401" : {
            "$ref" : "#/components/responses/unauthorized"
          },
          "404" : {
            "$ref" : "#/components/responses/not-found"
          },
          "406" : {
            "$ref" : "#/components/responses/not-acceptable"
          },
          "429" : {
            "$ref" : "#/components/responses/too-many-requests"
          },
          "500" : {
            "$ref" : "#/components/responses/internal-server-error"
          }
        }
      }
    },
    "/people/{id}/jobs" : {
      "get" : {
        "description" : "Get jobs for a person.",
        "operationId" : "get person jobs",
        "tags" : [ "people" ],
        "parameters" : [ {
          "$ref" : "#/components/parameters/id"
        }, {
          "$ref" : "#/components/parameters/fields"
        } ],
        "responses" : {
          "200" : {
            "$ref" : "#/components/responses/jobs"
          },
          "400" : {
            "$ref" : "#/components/responses/bad-request"
          },
          "401" : {
            "$ref" : "#/components/responses/unauthorized"
          },
          "404" : {
            "$ref" : "#/components/responses/not-found"
          },
          "406" : {
            "$ref" : "#/components/responses/not-acceptable"
          },
          "429" : {
            "$ref" : "#/components/responses/too-many-requests"
          },
          "500" : {
            "$ref" : "#/components/responses/internal-server-error"
          }
        }
      }
    }
  },
  "components" : {
    "securitySchemes" : {
      "OAuth2ClientCredentials" : {
        "type" : "oauth2",
        "flows" : {
          "clientCredentials" : {
            "tokenUrl" : "enter-url-here",
            "scopes" : { }
          }
        }
      }
    },
    "schemas" : {
      "people" : {
        "description" : "A person",
        "type" : "object",
        "properties" : {
          "type" : {
            "type" : "string",
            "example" : "people"
          },
          "id" : {
            "$ref": "#/components/schemas/personId"
          },
          "attributes" : {
            "type" : "object",
            "properties" : {
              "firstName" : {
                "type" : "string",
                "example" : "John",
                "description": "First name, derived from the person's current UW affiliations."
              },
              "lastName" : {
                "type" : "string",
                "example" : "Smith",
                "description": "Last name, derived from the person's current UW affiliations."
              },
              "emailAddress" : {
                "type" : "string",
                "example" : "john.smith@wisc.edu",
                "description": "The email address this person has designated as their primary, if set.  Otherwise, their Campus Business Email, or other email derived from their current UW affiliations."
              },
              "officeAddress" : {
                "type" : "string",
                "description": "Primary work-related address derived from ther person's current UW affiliations.  May or may not be a fully-formed street address.",
                "example": "1210 W. Dayton Street Madison, WI 53706-1613"
              },
              "officePhoneNumber" : {
                "type" : "string",
                "example" : "608-262-1204",
                "description": "Primary work-related phone number derived from the person's current UW affiliations.  May or may not be associated with the officeAddress."
              }
            }
          },
          "relationships" : {
            "type" : "object",
            "properties" : {
              "identifiers" : {
                "$ref" : "#/components/schemas/relationshipObject"
              },
              "jobs" : {
                "$ref" : "#/components/schemas/relationshipObject"
              }
            }
          },
          "links" : {
            "$ref" : "#/components/schemas/selfLink"
          },
          "meta": {
            "type": "object",
            "properties": {
              "ferpa": {
                "type": "boolean",
                "example": false,
                "description": "Indicates whether or not a student has elected total FERPA coverage. Please see https://registrar.wisc.edu/ferpa/ for more details about how to handle FERPA data."
              },
              "ferpaAttributes": {
                "type" : "array",
                "description": "Attributes a person has elected to be protected by FERPA coverage. Please see https://registrar.wisc.edu/ferpa/ for more details about how to handle FERPA data and see the example for all the possible values of this field.",
                "example": ["officeAddress", "officePhoneNumber"],
                "items" : {
                  "type": "string",
                  "enum": [
                    "officeAddress",
                    "officePhoneNumber"
                  ]
                }
              },
              "privateAttributes": {
                "type" : "array",
                "description": "Attributes a person has requested to be kept private.  See the example for all the possible values of this field.",
                "example": ["emailAddress","officeAddress", "officePhoneNumber"],
                "items" : {
                  "type": "string",
                  "enum": [
                    "emailAddress",
                    "officeAddress",
                    "officePhoneNumber"
                  ]
                }
              }
            }
          }
        }
      },
      "identifiers" : {
        "description" : "An identifier",
        "type" : "object",
        "properties" : {
          "type" : {
            "type" : "string",
            "example" : "identifiers"
          },
          "id" : {
            "$ref": "#/components/schemas/genericId"
          },
          "attributes" : {
            "type" : "object",
            "properties" : {
              "name" : {
                "type" : "string",
                "example" : "pvi"
              },
              "source" : {
                "type" : "string",
                "example" : "IAM"
              },
              "value" : {
                "type" : "string",
                "example" : "UW123A456"
              }
            }
          },
          "relationships" : {
            "type" : "object",
            "properties" : {
              "person" : {
                "$ref" : "#/components/schemas/relationshipObject"
              }
            }
          }
        }
      },
      "jobs" : {
        "description" : "A job",
        "type" : "object",
        "properties" : {
          "type" : {
            "type" : "string",
            "example" : "jobs"
          },
          "id" : {
            "$ref": "#/components/schemas/genericId"
          },
          "attributes" : {
            "type" : "object",
            "properties" : {
              "title" : {
                "type" : "string",
                "example" : "Software Engineer I",
                "description": "Business title of the person's position at this job."
              },
              "division" : {
                "type": "string",
                "example": "INFORMATION TECHNOLOGY",
                "description": "High level organizational unit where the job is located."
              },
              "divisionCode" : {
                "type" : "string",
                "example" : "A067040",
                "description": "Code for the location of the job."
              },
              "departmentHierarchy" : {
                "type": "string",
                "example": "L&S/COMPUTER SCI/COMP SCI",
                "description": "Organization path where the job is located."
              }
            }
          },
          "relationships" : {
            "type" : "object",
            "properties" : {
              "person" : {
                "$ref" : "#/components/schemas/relationshipObject"
              }
            }
          }
        }
      },
      "errors" : {
        "description" : "An error",
        "type" : "object",
        "properties" : {
          "status" : {
            "type" : "integer",
            "example" : 400
          },
          "title" : {
            "type" : "string",
            "example" : "Error title"
          },
          "detail" : {
            "type" : "string",
            "example" : "Additional details about the error"
          },
          "links" : {
            "type" : "object"
          },
          "meta" : {
            "type" : "object"
          },
          "source" : {
            "type" : "object"
          }
        }
      },
      "selfLink" : {
        "description" : "The link to access a resource itself",
        "type" : "object",
        "properties" : {
          "self" : {
            "type" : "string",
            "example" : "https://api.wisc.edu/link/to/self"
          }
        }
      },
      "pagination" : {
        "description" : "The links associated with a paginated collection",
        "type" : "object",
        "properties" : {
          "prev" : {
            "description" : "The previous page of data",
            "type" : "string",
            "format" : "uri-reference",
            "example" : "https://api.wisc.edu/people?page[number]=4"
          },
          "next" : {
            "description" : "The next page of data",
            "type" : "string",
            "format" : "uri-reference",
            "example" : "https://api.wisc.edu/people?page[number]=6"
          }
        }
      },
      "relationshipObject" : {
        "description" : "A generic relationship object",
        "type" : "object",
        "properties" : {
          "links" : {
            "type" : "object",
            "properties" : {
              "related" : {
                "type" : "string",
                "example" : "https://api.wisc.edu/link/to/related/object"
              }
            }
          },
          "data" : {
            "type" : "array",
            "items" : {
              "type" : "object",
              "properties" : {
                "type" : {
                  "type" : "string",
                  "example" : "sampleType"
                },
                "id" : {
                  "type" : "string",
                  "example" : "sampleId"
                }
              }
            }
          }
        }
      },
      "resourceObject" : {
        "description" : "A generic resource object",
        "type" : "object",
        "properties" : {
          "type" : {
            "type" : "string",
            "example" : "sampleType"
          },
          "id" : {
            "type" : "string",
            "example" : "sampleId"
          },
          "attributes" : {
            "type" : "object",
            "properties" : {
              "attribute1" : {
                "type" : "string",
                "example" : "sampleStringAttribute"
              },
              "attribute2" : {
                "type" : "number",
                "example" : 123
              }
            }
          },
          "relationships" : {
            "type" : "object",
            "properties" : {
              "relatedResource" : {
                "$ref" : "#/components/schemas/relationshipObject"
              }
            }
          },
          "links" : {
            "$ref" : "#/components/schemas/selfLink"
          }
        }
      },
      "personId" : {
        "type": "string",
        "description": "An ephemeral identifier used only in the Person API to adhere to the [JSON:API specification](https://jsonapi.org/format/#document-resource-object-identification). This ID may change if a person is split into two people or two people are merged into one person. This ID is used for paginating results or following other links returned in the API response. This ID should not be stored in a consuming system. Concatenating parts of the URL to form a link is discouraged. Instead, use the fully-formed links returned in an API response.",
        "example": "12345"
      },
      "genericId" : {
        "type": "string",
        "example": "12345",
        "description": "Ephemeral ID used to adhere to the JSON:API specification. This ID should not be stored."
      }
    },
    "responses" : {
      "people" : {
        "description" : "A collection of people",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "data" : {
                  "description" : "A collection of people",
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/people"
                  }
                },
                "links" : {
                  "description" : "Self and pagination links",
                  "allOf" : [ {
                    "$ref" : "#/components/schemas/selfLink"
                  }, {
                    "$ref" : "#/components/schemas/pagination"
                  } ]
                },
                "included" : {
                  "description" : "Included related resources",
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/resourceObject"
                  }
                }
              }
            },
            "examples": {
              "example-people" : {
                "value": {
                  "data": [
                    {
                      "attributes": {
                        "emailAddress": "john.smith@wisc.edu",
                        "firstName": "John",
                        "lastName": "Smith"
                      },
                      "id": "100",
                      "links": {
                        "self": "https://mock.api.wisc.edu/people/100"
                      },
                      "meta": {
                        "ferpa": false
                      },
                      "relationships": {
                        "identifiers": {
                          "data": [
                            {
                              "id": "100",
                              "type": "identifiers"
                            }
                          ],
                          "links": {
                            "related": "https://mock.api.wisc.edu/people/100/identifiers"
                          }
                        },
                        "jobs": {
                          "data": [
                            {
                              "id": "100",
                              "type": "jobs"
                            }
                          ],
                          "links": {
                            "related": "https://mock.api.wisc.edu/people/100/jobs"
                          }
                        }
                      },
                      "type": "people"
                    }
                  ],
                  "links": {
                    "next": null,
                    "prev": null,
                    "self": "https://mock.api.wisc.edu/people"
                  }
                }
              }
            }
          }
        }
      },
      "people-id" : {
        "description" : "A single person",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "data" : {
                  "$ref" : "#/components/schemas/people"
                },
                "links" : {
                  "$ref" : "#/components/schemas/selfLink"
                },
                "included" : {
                  "description" : "Included related resources",
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/resourceObject"
                  }
                }
              }
            },
            "examples": {
              "example-people" : {
                "value": {
                  "data":
                    {
                      "attributes": {
                        "emailAddress": "john.smith@wisc.edu",
                        "firstName": "John",
                        "lastName": "Smith"
                      },
                      "id": "100",
                      "links": {
                        "self": "https://mock.api.wisc.edu/people/100"
                      },
                      "meta": {
                        "ferpa": false
                      },
                      "relationships": {
                        "identifiers": {
                          "data": [
                            {
                              "id": "100",
                              "type": "identifiers"
                            }
                          ],
                          "links": {
                            "related": "https://mock.api.wisc.edu/people/100/identifiers"
                          }
                        },
                        "jobs": {
                          "data": [
                            {
                              "id": "100",
                              "type": "jobs"
                            }
                          ],
                          "links": {
                            "related": "https://mock.api.wisc.edu/people/100/jobs"
                          }
                        }
                      },
                      "type": "people"
                    }
                  ,
                  "links": {
                    "self": "https://mock.api.wisc.edu/people"
                  }
                }
              }
            }
          }
        }
      },
      "identifiers" : {
        "description" : "A collection of identifiers.  These may be: 'pvi' (Publicly Visible Identifier, a unique value from DoIT Identity and Access Management), 'netId' (username for the NetID Login Service), or 'emplId' (from Human Resources System - HRS - for employees, or from Student Information System - SIS -for students).",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "data" : {
                  "description" : "A collection of identifiers",
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/identifiers"
                  }
                },
                "links" : {
                  "description" : "Self and pagination links",
                  "allOf" : [ {
                    "$ref" : "#/components/schemas/selfLink"
                  }, {
                    "$ref" : "#/components/schemas/pagination"
                  } ]
                },
                "included" : {
                  "description" : "Included related resources",
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/resourceObject"
                  }
                }
              }
            },
            "examples": {
              "example-identifiers" : {
                "value": {
                  "data": [
                    {
                      "attributes": {
                        "name": "pvi",
                        "source": "IAM",
                        "value": "UW123A456"
                      },
                      "id": "100",
                      "relationships": {
                        "person": {
                          "data": {
                            "id": "100",
                            "type": "people"
                          },
                          "links": {
                            "related": "https://mock.api.wisc.edu/people/100"
                          }
                        }
                      },
                      "type": "identifiers"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "jobs" : {
        "description" : "A collection of jobs",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "data" : {
                  "description" : "A collection of jobs",
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/jobs"
                  }
                },
                "links" : {
                  "$ref" : "#/components/schemas/selfLink"
                },
                "included" : {
                  "description" : "Included related resources",
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/resourceObject"
                  }
                }
              }
            },
            "examples": {
              "example-jobs" : {
                "value": {
                  "data":  [
                    {
                      "attributes": {
                        "departmentUnit": "DOIT/AIS/ENTERPRISE INTEGRAT",
                        "division": "INFORMATION TECHNOLOGY",
                        "divisionCode": "A067040",
                        "title": "Product Manager"
                      },
                      "id": "100",
                      "relationships": {
                        "person": {
                          "data": {
                            "id": "100",
                            "type": "people"
                          },
                          "links": {
                            "related": "https://mock.api.wisc.edu/people/100"
                          }
                        }
                      },
                      "type": "jobs"
                    }
                  ]

                }
              }
            }
          }
        }
      },
      "not-found" : {
        "description" : "Unable to find the specified record",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "errors" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/errors"
                  }
                }
              }
            },
            "examples" : {
              "not-found-example" : {
                "value" :
                {
                  "errors": [
                    {
                      "status": 404,
                      "title": "Not Found",
                      "detail": "Requested resource not found."
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "not-acceptable" : {
        "description" : "Servers respond with a 406 Not Acceptable status code if a request’s Accept header contains the JSON:API media type and all instances of that media type are modified with media type parameters.",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "errors" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/errors"
                  }
                }
              }
            },
            "examples" : {
              "not-found-example" : {
                "value" :
                {
                  "errors": [
                    {
                      "status": 406,
                      "title": "Not Acceptable",
                      "detail": "Request's Accept header contained the JSON:API media type and all instances of that media type were modified with media type parameters."
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "too-many-requests" : {
        "description" : "The API quota has been exceeded.",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "errors" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/errors"
                  }
                }
              }
            },
            "examples" : {
              "not-found-example" : {
                "value" :
                {
                  "errors": [
                    {
                      "status": 429,
                      "title": "Too Many Requests",
                      "detail": "The quota has been exceeded."
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "unauthorized" : {
        "description" : "Incorrect or expired OAuth token.",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "errors" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/errors"
                  }
                }
              }
            },
            "examples" : {
              "unauthorized-response" : {
                "value" :
                {
                  "errors": [
                    {
                      "status": 401,
                      "title": "Unauthorized",
                      "detail": "Invalid OAuth authentication - InvalidAccessToken"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "internal-server-error" : {
        "description" : "An unexpected error.",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "errors" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/errors"
                  }
                }
              }
            },
            "examples" : {
              "not-found-example" : {
                "value" :
                {
                  "errors": [
                    {
                      "status": 500,
                      "title": "Internal Server Error",
                      "detail": "Something went wrong in the server-side."
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "bad-request" : {
        "description" : "A bad request.",
        "content" : {
          "application/vnd.api+json" : {
            "schema" : {
              "type" : "object",
              "properties" : {
                "errors" : {
                  "type" : "array",
                  "items" : {
                    "$ref" : "#/components/schemas/errors"
                  }
                }
              }
            },
            "examples" : {
              "application/vnd.api+json" : {
                "value" :
                {
                  "errors": [
                    {
                      "status": 400,
                      "title": "Bad Request",
                      "detail": "Invalid properties in query parameters - resource type 'people' has no attribute 'invalid'."
                    }
                  ]
                }
              }
            }
          }
        }
      }
    },
    "parameters" : {
      "id" : {
        "name" : "id",
        "description" : "ID of a person. This parameter should not be manually built/concatenated, but instead be used by following the fully-formed links returned in an API response. To get a person by an ID, use query parameters to search by a specific identifier. Example: /people?filter[identifiers.name]=netId&filter[identifiers.value]=bbadger",
        "in" : "path",
        "example": "100",
        "required" : true,
        "schema" : {
          "$ref": "#/components/schemas/personId"
        }
      },
      "filter" : {
        "name" : "filter",
        "in" : "query",
        "description" : "Filter results of query using the [filter query parameter family](https://jsonapi.org/recommendations/#filtering).  The attribute name is case sensitive (example: `lastName`) but the value is not (example: `smith` and `SMITH` will match `Smith`). This is used for the common crosswalk type workflow where you run a query such as `filter[identifiers.name]=netId&filter[identifiers.value]=exampleNetId&include=identifiers` to find a person and their other identifiers from a single identifier. Multiple values can be included in a filter by using a comma as a delimiter, which will execute an \"OR\" query for the included values (e.g. `/people?filter[firstName]=Jane,Jon` will find people that have the firstName of Jane or Jon). There is a limit of 10 values per filter. Multiple filters for the same attribute cannot be sent in a single request (e.g. `/people?filter[firstName]=Jane&filter[firstName]=Jon`). Instead, use a single filter with comma-separated values. Use a backslash (`\\`) to escape a comma, if the filter value contains a comma that shouldn't be used as a delimiter. For example, `/people?filter[officeAddress]=123 Main St\\, Madison` will find people with the office address \"123 Main St, Madison\", but `/people?filter[officeAddress]=123 Main St, Madison` would find people who have the office address \"123 Main St\" or \" Madison\"",
        "required" : false,
        "style" : "deepObject",
        "explode": true,
        "schema" : {
          "type" : "object",
          "additionalProperties" : false,
          "properties": {
            "attribute": {
              "type": "string",
              "example": "firstName"
            }
          }
        }
      },
      "include" : {
        "name" : "include",
        "in" : "query",
        "description" : "[include related resources](https://jsonapi.org/format/#fetching-includes). The resources to include need to be direct relationships. Nested relationships are not supported. For example, `include=jobs,identifiers` is supported but `include=person.jobs` is not supported.",
        "schema" : {
          "type" : "string"
        }
      },
      "fields" : {
        "name" : "fields",
        "in" : "query",
        "description" : "Specify the list of fields you would like to return for each resource type.  For example, `fields[people]=firstName,lastName` will just return the names for people.  If you are including related resources with 'includes' you can also specify fields on those resources as well.  For example, `includes=jobs&fields[people]=jobs&fields[jobs]=title` will just return titles for jobs.  See [Sparse Fieldsets](https://jsonapi.org/format/#fetching-sparse-fieldsets) for more details.",
        "required" : false,
        "style" : "deepObject",
        "explode": true,
        "schema" : {
          "type" : "object",
          "additionalProperties" : false,
          "properties": {
            "type": {
              "type": "string",
              "example": "people"
            }
          }
        }
      },
      "page" : {
        "name" : "page",
        "in" : "query",
        "description" : "Define options for pagination of responses. For example, `page[size]=5&page[after]=400&page[before]=800` will return a response with data about 5 people whose `id` value falls after `400` but before `800`. So `page[size]` determines the number of people resources in a response, `page[after]` is the ID of the person at the end of the previous page, `page[before]` is the ID of the person at the start of the next page.",
        "required" : false,
        "style" : "deepObject",
        "explode": true,
        "schema" : {
          "type" : "object",
          "properties": {
            "size": {
              "type": "integer",
              "example": 50,
              "description": "Size of page for paginated results."
            },
            "after": {
              "$ref": "#/components/schemas/personId"
            },
            "before": {
              "$ref": "#/components/schemas/personId"
            }
          }
        }
      }
    }
  }
}
